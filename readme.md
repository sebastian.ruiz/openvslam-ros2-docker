# Openvslam ROS 2 in Docker

Docker image by [Mirella Melo](https://github.com/mirellameelo/openvslam/tree/ros2-base)

## Installation

1. Run container:
```
docker-compose up -d

```

2. Once built, sh into container:
```
docker exec -ti openvslam /bin/bash
```

3. Done!

If something failed in the build process, it may be necessary to run these commands:
```
source /opt/ros/dashing/setup.bash
cd $HOME/openvslam/ros2
colcon build --symlink-install
source $HOME/openvslam/ros2/install/setup.bash
```

To rebuild image run:
```
docker-compose build
```

## Run Example

Terminal 1:
```
ros2 run publisher video -m $HOME/openvslam/videos/aist_living_lab_1/video.mp4
```

Terminal 2:
```
ros2 run openvslam run_slam -v $HOME/openvslam/Vocabulary/orb_vocab/orb_vocab.dbow2 -c $HOME/openvslam/videos/aist_living_lab_1/config.yaml
```